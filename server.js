const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const path = require('path')
const apiDitoRoutes = require('./api/dito/route')
const apiEventsRoutes = require('./api/event/route')
const config = require('config')
const swaggerDocument = require('./swagger.json')
const swaggerUi = require('swagger-ui-express')

var app = express()

app.use(bodyParser.urlencoded({'extended':'true'}))
app.use(bodyParser.json())
app.use(morgan(':method :url :response-time'))

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.use('/api/dito', apiDitoRoutes)
app.use('/api/events', apiEventsRoutes)

app.set('port', config.get('APP.PORT'))
app.listen(app.get('port'), () => {
    console.log(`api listening on port ${app.get('port')}`)
})