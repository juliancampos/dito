const request = require('request')
const config = require('config')
const apiDitoQuestions = config.get('DITO.URL_TIMELINE')

const Service = {
    getTimeLine
}

function formatingResponse(bodyJson, callback) {
    var arrayComprou = bodyJson.events.filter(function(comprou){
        if (comprou.event == "comprou") {
            comprou.transaction_id = comprou.custom_data[0].value
            comprou.store_name = comprou.custom_data[1].value
            comprou.products = []
            delete comprou.event
            delete comprou.custom_data
            return comprou
        }
    })

    var arrayComprouProduto = bodyJson.events.filter(function(comprouProduto){
        return comprouProduto.event == "comprou-produto"
    })

    for(var i in arrayComprou) {
        var arrayTeste = arrayComprouProduto.filter(function(teste){
            if (arrayComprou[i].transaction_id == teste.custom_data[0].value) {
                arrayComprou[i].products.push({
                    'name': teste.custom_data[1].value,
                    'price': teste.custom_data[2].value
                })
            }
        })
    }

    arrayComprou.sort(function compare(a, b){
        return (a.timestamp < b.timestamp)
    })
    callback({'timeline' : arrayComprou})
}

function getTimeLine(callback) {
    request(apiDitoQuestions, function(error, response, body){
        var bodyJson = JSON.parse(body)
        formatingResponse(bodyJson, function(result){
            callback(result)
        })
    })
}

module.exports = function factory() {
    return Service
}