const Service = require('./service')
const service = Service()

const Controller = {
    getTimeLine
}

function getTimeLine(req, resp) {
    service.getTimeLine(function(result) {
        resp.send(result)
    })
}

module.exports = function factory() {
    return Controller
}