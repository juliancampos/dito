const express = require('express')
const Controller = require('./controller')

const router = express.Router()
const controller = Controller()

router.route('/save')
    .post(controller.save)

router.route('/get')
    .get(controller.get)

router.route('/getAutoComplete/:search')
    .get(controller.getAutoComplete)


module.exports = router