const express = require('express')
const Controller = require('./controller')

const router = express.Router()
const controller = Controller()

router.route('/timeline')
    .get(controller.getTimeLine)

module.exports = router