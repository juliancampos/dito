const Database = require('./database')
const database = Database()

const Controller = {
    save,
    get,
    getAutoComplete
}

function save(req, resp) {
    try {
        var event = {
            "event": req.body.event,
            "timestamp": req.body.timestamp
        }
        database.save(event, function(result){
            resp.status(201).send(result)
        })
    } catch(error){
        resp.status(404).send(error)
    }
}

function get(req, resp) {
    try {
        database.get({}, function(result){
            resp.status(200).send(result)
        })
    } catch(error) {
        resp.status(404).send(error)
    }
}

function getAutoComplete(req, resp) {
    try {
        if (req.params.search.length > 1) {
            var regex = new RegExp("^"+req.params.search)
            database.get({'event': regex}, function(result){
                resp.status(200).send(result)
            })
        } else {
            resp.status(200).send(null)
        }
    } catch(error) {
        resp.status(404).send(error)
    }
}

module.exports = function factory() {
    return Controller
}