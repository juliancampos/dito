var Mongoose = require('mongoose')
var db = Mongoose.connection

var eventSchema = new Mongoose.Schema({
    event: String,
    timestamp: String
})

module.exports = Mongoose.model('events', eventSchema)