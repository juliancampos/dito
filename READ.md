
## Teste Dito ##
Software desenvolvido para o processo seletivo Dito

## Informações ##

### Principais recursos utilizados: ###
* NodeJS
* Framework Express
* Banco de Dados MongoDB
* Swagger
* yaml
* Mongoose

### Estrutura ###
O projeto foi estruturado para que seja possível a configuração da porta de acesso ao sistema,
link de conexão com o banco de dados e url para a busca da timeline sem que sejam realizadas
grandes alterações no projeto, para isso, deve ser alterado o arquivo 'default.yml' que se 
encontra na pasta 'config'.

Desenvolvida configuração com a ferramenta Swagger para que sirva de auxilio, teste e documentação
do software, sendo possível realizar chamadas às rotas da Api. Sua configuração se da através do 
arquivo 'swagger.json' que se encontra na raiz do projeto.

As rotas da api foram modularizadas para que possam ser criadas novas rotas/modulos em pastas 
separadas, estas se encontram na pasta './api'
Cada módulo contém seus controllers, services e routes.

Para conexão com o banco de dados MongoDB, que foi escolhido visando a escalabilidade do projeto,
foi utilizado o recurso Mongoose

#### Execução ####
Para execução do software devem ser instalados Node e mongodb.
Após realizar o download do software, deve ser executado o comando:
* node server.js

#### Rotas ####
* Acesso ao Swagger: localhost:3002/swagger