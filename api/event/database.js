const config = require('config')
const urlConnection = config.get('DATABASE.URL_CONNECTION')

var Mongoose = require('mongoose')
var db = Mongoose.connection

var EventModel = require('./eventModel')
const database = {
    save,
    get
}

function save(event, callback) {
    db.once('open', function () {
        var newEvent = new EventModel(event)
        newEvent.save(function(error, newEvent){
            if (error) return console.log(error)
            callback(newEvent)
        })
    })
    Mongoose.connect(urlConnection)
}

function get(event, callback) {
    db.once('open', function() {
        EventModel.find(event, function(error, events) {
            if (error) return console.log(error)
            callback(events)
        })
    })
    Mongoose.connect(urlConnection)
}

module.exports = function factory() {
    return database
}